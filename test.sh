
#!/bin/bash

set -e

for file in $(find ./src -type f -name '*_test.adoc'); do
  echo -n "$file: "
  ./build.sh -D tmp/ "$file" && echo "OK"
done